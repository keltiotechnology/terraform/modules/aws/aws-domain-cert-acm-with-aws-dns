variable "domain_name" {
  type        = string
  description = "The domain name alias to the Application Load balancer"
}

variable "subject_alternative_names" {
  type        = list(string)
  description = "DNS name of AWS Load balancer"
  default     = []
}

variable "hosted_zone_id" {
  type        = string
  description = "ID of hosted zone in AWS Route53 (Required when dns_provider is aws)"
  default     = ""
}
